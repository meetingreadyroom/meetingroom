import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Stack(
          children: <Widget>[
            Container(
              color: Colors.black.withOpacity(0.7),
            ),
            SafeArea(
                child: SingleChildScrollView(
                    padding: EdgeInsets.all(46.0),
                    child: Container(
                      child: Align(
                        alignment: Alignment.center,
                        child: Column(
//                  mainAxisAlignment: MainAxisAlignment.center,
//                  crossAxisAlignment: CrossAxisAlignment.start,
//                  mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            SizedBox(height: 100),
                            Text(
                              "Welcome",
                              style: TextStyle(
                                  color: Colors.white, fontSize: 28.0),
                            ),
                            SizedBox(height: 32),
                            TextField(
                              style: TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                  hintText: "Email",
                                  hintStyle: TextStyle(color: Colors.grey),
                                  enabledBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white54))),
                            ),
                            SizedBox(height: 16),
                            TextField(
                              obscureText: true,
                              style: TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                  hintText: "Password",
                                  hintStyle: TextStyle(color: Colors.grey),
                                  enabledBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white54))),
                            ),
                            SizedBox(height: 40),
                            GestureDetector(
                              onTap: () {},
                              child: Text(
                                "Forgot your password?",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                            SizedBox(
                              width: double.infinity,
                              child: RaisedButton(
                                child: Text("Sign in".toUpperCase()),
                                onPressed: () {},
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "Dont have an account?",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16.0),
                                ),
                                SizedBox(
                                  width: 10.0,
                                ),
                                GestureDetector(
                                  onTap: () {},
                                  child: Text(
                                    "SIGN UP",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 16.0),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ))),
          ],
        ),
      ),
    );
  }
}
