import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';

class BaseRepo {
  final fbFirestore = Firestore.instance;
  final fbDatabase = FirebaseDatabase();
}